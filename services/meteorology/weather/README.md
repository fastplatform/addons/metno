# `metno`: Met.no weather service add-on based on the https://met.no public APIs

This service exposes weather forecasts to the FaST users and is based on public data exposed by [Met.no](https://api.met.no).

It is composed of a GraphQL server that exposes a schema compliant with the *FaST Weather GraphQL ontology*. The GraphQL server is coded using the [Graphene](https://graphene-python.org/) framework + [FastAPI](https://fastapi.tiangolo.com/)/[Starlette](https://www.starlette.io/) and served behind a [uvicorn](https://www.uvicorn.org/) server.

The service exposes 3 types of objects:

- weather observations (past data): not implemented, will return `[]`
- weather forecasts: fetched from the [LocationForecast API from Met.no](https://api.met.no/weatherapi/locationforecast/2.0/documentation)
- weather alerts: not implemented, will return `[]`

*This service is not in any way endorsed by The Norwegian Meteorological Institute.*

## Architecture

The service is composed of a GraphQL server that exposes a schema compliant with the *FaST Weather GraphQL ontology*. The GraphQL server is implemented using the [Graphene](https://graphene-python.org/) framework + [fastapi](https://fastapi.tiangolo.com/)/[Starlette](https://www.starlette.io/) and served behind a [uvicorn](https://www.uvicorn.org/) server.

```plantuml

agent "Farmer app" as farmer
component "FaST API Gateway" as api #line.dashed
component "Web server" as web {
    storage "In-memory\ncache" as cache
}
cloud {
    component """api.met.no""\nLocationForecast API" as metno
    component """nominatim.openstreetmap.org""\nReverse geocoding API" as nominatim
}

farmer --> api      : /graphql
api --> web         : /graphql
web --> metno
web --> nominatim

```

## GraphQL server

The service exposes a GraphQL server on the `/graphql` endpoint. The following (high-level) GraphQL schema is exposed:

```graphql
query {
    weather {
        # Weather forecasts (future)
        forecasts_implemented
        forecasts_interval_types
        forecast(geometry, interval_type) {
            id
            sample_type
            interval_type
            location {
                id
                authority
                authority_id
                geometry
                name
            }
            valid_from
            valid_to
            computed_at
            fetched_at
            origin {
                id
                name
                url
                icon
            }
            summary
            icon
            nearest_storm_distance
            nearest_storm_bearing
            precipitation_intensity
            precipitation_intensity_error
            precipitation_probability
            precipitation_type
            temperature
            temperature_min
            temperature_max
            temperature_min_at
            temperature_max_at
            apparent_temperature
            dew_point
            humidity
            humidity_min
            humidity_max
            humidity_min_at
            humidity_max_at
            pressure
            wind_speed
            wind_speed_max
            wind_speed_max_at
            wind_speed_max_bearing
            wind_gust
            wind_bearing
            cloud_cover
            uv_index
            radiation
            visibility
            ozone
            soil_temperatures {
                id
                depth
                temperature
            }
            evapotranspiration
            source
        }

        # Weather observations (past)
        observations_implemented  # = false
        observations_interval_types
        observations(geometry, interval_type, date_from, date_to) {
            # ... same fields as the `forecast` node
        }
        
        # Weather alerts
        alerts_implemented  # = false
        alerts(geometry) {
            id
            sender
            issued_at
            title
            summary
            description
            url
            category
            urgency
            severity
            certainty
            location
            valid_from
            valid_to
        }
    }
}

# The service does not expose any mutation or subscription.
```

The schema is self documenting and we encourage the reader to introspect it to learn more about each node and its associated fields and relations. This can done using the [GraphiQL](https://github.com/graphql/graphiql) or [Altair](https://altair.sirmuel.design/) tools, for example.

<div align="center">
    <img src="docs/img/graphql-schema.png" width="420">
</div>

A sample query is provided, as part of the test suite: [app/tests/]().

### Environment variables

For the Met.no API (weather forecasts):
- `METNO_USER_AGENT`: User-Agent header that will be sent with every request to identify the FaST backend
- `METNO_DEFAULT_TIMEOUT`: Timeout of calls to the [met.no API](https://api.met.no) (defaults to `10` seconds)
- `METNO_LOCATION_FORECAST_URL`: URL of the Met.no LocationForecast endpoint (defaults to `https://api.met.no/weatherapi/locationforecast/2.0`)
- `METNO_LOCATION_FORECAST_CACHE_MAX_SIZE`: Maximum number of hourly forecasts API calls that are cached in memory (defaults to `10000`)
- `METNO_LOCATION_FORECAST_CACHE_TTL`: Time-to-live of the hourly forecasts cache entries (defaults to `300` seconds)
- `METNO_LOCATION_FORECAST_ORIGIN_NAME`: Origin name that will be inserted in the GraphQL response (defaults to `Based on data from MET Norway`)
- `METNO_LOCATION_FORECAST_ORIGIN_URL`: Origin URL that will be inserted in the GraphQL response (defaults to `https://met.no`)

For the Nominatim API (reverse geocoding, to determine the location name from a latitude/longitude):
- `NOMINATIM_REVERSE_GEOLOCATION_URL`: URL of the Nominatim reverse geocoding endpoint (defaults to `https://nominatim.openstreetmap.org/reverse`)
- `NOMINATIM_REVERSE_GEOLOCATION_DEFAULT_TIMEOUT`: Timeout of calls to the Nominatim reverse geocoding endpoint (defaults to `10` seconds) 
- `NOMINATIM_REVERSE_GEOLOCATION_USER_AGENT`: User-Agent header that will be sent with every Nominatim request to identify the FaST backend 
- `NOMINATIM_REVERSE_GEOLOCATION_CACHE_MAX_SIZE`: Maximum number of reverse geocoding API calls that are cached in memory (defaults to `10000`)
- `NOMINATIM_REVERSE_GEOLOCATION_CACHE_TTL`: Time-to-live of the reverse geocoding cache entries (defaults to `300` seconds)

## Development 

To run the service on your local machine, ensure you have the following dependencies installed:
- [Python 3.7+](https://www.python.org/) with the `virtualenv` package installed
- The `make` utility

Create a virtual environment and activate it:
```bash
virtualenv .venv
source .venv/bin/activate
```

Install the Python dependencies:
```bash
pip install -r requirements.txt
pip install -r requirements-dev.txt  # optional
```

Start the development server with auto-reload enabled:
```bash
make start
```

The GraphQL endpoint is now available at `http://localhost:7777/graphql`.

### Management commands

The following management commands are available from the `Makefile`.

- Uvicorn targets:
    - `make start`: Start the development server with auto-reload enabled.

- Testing targets:
    - `make test`: Run the test query
	
Some additional commands are available in the `Makefile`, we encourage you to read it (or to type `make help` to get the full list of commands).

## Running the tests

Start the server (if it is not already running):
```bash
make start
```

In another shell, run the test query:
```bash
make test
```
