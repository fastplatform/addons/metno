# As per https://api.met.no/weatherapi/weathericon/2.0/documentation

# The second (numeric) value is a weight that is used to determine which icon
# is retained for the daily summaries.

_metno_icons_mapping = {
    "clearsky_day": "clear_day",
    "clearsky_night": "clear_night",
    "cloudy": "cloudy",
    "fair_day": "partly_cloudy_day",
    "fair_night": "partly_cloudy_night",
    "fog": "fog",
    "heavyrain": "rain",
    "heavyrainandthunder": "thunderstorm",
    "heavyrainshowers": "rain",
    "heavyrainshowersandthunder": "thunderstorm",
    "heavysleet": "rain",
    "heavysleetandthunder": "thunderstorm",
    "heavysleetshowers": "rain",
    "heavysleetshowersandthunder": "thunderstorm",
    "heavysnow": "snow",
    "heavysnowandthunder": "snow",
    "heavysnowshowers": "snow",
    "heavysnowshowersandthunder": "snow",
    "lightrain": "light_rain",
    "lightrainandthunder": "thunderstorm",
    "lightrainshowers": "light_rain",
    "lightrainshowersandthunder": "thunderstorm",
    "lightsleet": "light_rain",
    "lightsleetandthunder": "thunderstorm",
    "lightsleetshowers": "light_rain",
    "lightssleetshowersandthunder": "thunderstorm",
    "lightsnow": "snow",
    "lightsnowandthunder": "snow",
    "lightsnowshowers": "snow",
    "lightssnowshowersandthunder": "snow",
    "partlycloudy_day": "partly_cloudy_day",
    "partlycloudy_night": "partly_cloudy_night",
    "rain": "rain",
    "rainandthunder": "thunderstorm",
    "rainshowers": "rain",
    "rainshowersandthunder": "thunderstorm",
    "sleet": "rain",
    "sleetandthunder": "thunderstorm",
    "sleetshowers": "rain",
    "sleetshowersandthunder": "thunderstorm",
    "snow": "snow",
    "snowandthunder": "snow",
    "snowshowers": "snow",
    "snowshowersandthunder": "snow"
}

def get_fastplatform_icon_from_metno_icon(metno_icon):
    if metno_icon in _metno_icons_mapping:
        return _metno_icons_mapping[metno_icon]
    metno_icon, _ = metno_icon.split('_')
    if metno_icon + "_day" in _metno_icons_mapping:
        return _metno_icons_mapping[metno_icon + "_day"]
    if metno_icon in _metno_icons_mapping:
        return _metno_icons_mapping[metno_icon]
    return None
