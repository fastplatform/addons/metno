import logging
from datetime import datetime, timedelta, timezone, date
from email.utils import parsedate_to_datetime
from collections import defaultdict
import pytz
import hashlib
import json
from math import cos, sin, radians, degrees, atan2

import httpx
from graphql import GraphQLError
import cachetools
from threading import Lock

from app.lib.metno_datatypes import MetNoLocationForecastResponse
from app.lib.metno_icons import get_fastplatform_icon_from_metno_icon
from app.settings import config
from app.api.types.weather import (
    Sample,
    SampleType,
    IntervalType,
    Location,
    PrecipitationType,
    Origin,
    Icon,
)

logger = logging.getLogger(__name__)

# Override the default TTLCache implementation to allow for
# per-item time-to-live
# As per https://stackoverflow.com/a/56911464
class TTLItemCache(cachetools.TTLCache):
    def __setitem__(
        self, key, value, cache_setitem=cachetools.Cache.__setitem__, ttl=None
    ):
        super(TTLItemCache, self).__setitem__(key, value)
        if ttl:
            # pylint: disable=maybe-no-member
            link = self._TTLCache__links.get(key, None)
            if link:
                link.expire += ttl - self.ttl


class MetNoClient:
    """A client to retrieve data from Met.no API"""

    def __init__(self):
        self.http_client = None

        self.forecasts_cache = TTLItemCache(
            maxsize=config.METNO_LOCATION_FORECAST_CACHE_MAX_SIZE,
            # The TTL value will anyway be overwritten per item on insert
            # based on the Expire header
            ttl=config.METNO_LOCATION_FORECAST_CACHE_TTL,
        )
        self.forecasts_cache_lock = Lock()

    def create_http_client(self):
        self.http_client = httpx.AsyncClient()

    async def close_http_client(self):
        await self.http_client.aclose()

    async def get_location_forecast(self, longitude, latitude):
        """Query weather forecasts from the Met.no API or from the cache

        Args:
            longitude (float): Longitude in degrees
            latitude (float): Latitude in degrees

        Raises:
            GraphQLError:
                - API_REMOTE_ERROR: remote API error
                - API_DATA_ERROR: data parsing error

        Returns:
            [Sample]: List of Sample GraphQL nodes
        """
        logger.debug("query_forecasts")

        # Reduce the longitude/latitude precision down to roughly 1km
        longitude = round(longitude, 2)
        latitude = round(latitude, 2)

        # Read and return from cache if entry exists
        with self.forecasts_cache_lock:
            samples = self.forecasts_cache.get((longitude, latitude))

        if samples is not None:
            return samples

        # Otherwise, query from the API
        try:
            response = await self.http_client.get(
                config.METNO_LOCATION_FORECAST_URL,
                params={"lon": longitude, "lat": latitude},
                timeout=config.METNO_DEFAULT_TIMEOUT,
                headers={"User-Agent": config.METNO_USER_AGENT},
            )
            response.raise_for_status()
        except Exception as e:
            logger.exception("Failed to retrieve forecasts")
            raise GraphQLError("API_REMOTE_ERROR") from e

        # Parse the API response
        samples, time_to_live = self.parse_location_forecast_response(response)

        # Set the values in the cache
        with self.forecasts_cache_lock:
            self.forecasts_cache.__setitem__(
                (longitude, latitude), samples, ttl=time_to_live
            )

        return samples

    def parse_location_forecast_response(self, response):
        """Parse the response from the Met.no API into the FaST ontology

        Args:
            response (dict): Dict/JSON response from the API

        Raises:
            GraphQLError:
                - API_DATA_ERROR: data parsing error

        Returns:
            [Sample]: List of Sample GraphQL nodes
            time_to_live: TTL in seconds
        """

        # Parse the response body
        try:
            data = MetNoLocationForecastResponse(**response.json())
        except Exception as e:
            logger.exception("Failed to parse forecasts")
            raise GraphQLError("API_DATA_ERROR") from e

        # Get the date at which the forecast was actually computed by Met.no
        computed_at = data.properties.meta.updated_at

        # Convert from Met.no data model to FaST Ontology
        hourly_samples = []
        for time_series_element in data.properties.timeseries:

            try:
                metno_icon = time_series_element.data.next_1_hours.summary.symbol_code
            except AttributeError:
                try:
                    metno_icon = (
                        time_series_element.data.next_6_hours.summary.symbol_code
                    )
                except AttributeError:
                    try:
                        metno_icon = (
                            time_series_element.data.next_12_hours.summary.symbol_code
                        )
                    except AttributeError:
                        metno_icon = None

            icon = (
                get_fastplatform_icon_from_metno_icon(metno_icon)
                if metno_icon is not None
                else None
            )

            try:
                precipitation_intensity = (
                    time_series_element.data.next_1_hours.details.precipitation_amount
                )
            except AttributeError:
                try:
                    precipitation_intensity = (
                        time_series_element.data.next_6_hours.details.precipitation_amount
                        / 6
                    )
                except AttributeError:
                    try:
                        precipitation_intensity = (
                            time_series_element.data.next_12_hours.details.precipitation_amount
                            / 12
                        )
                    except AttributeError:
                        precipitation_intensity = None

            # Try to infer the precipitation type from the met.no/FaST symbol
            if (
                precipitation_intensity is not None
                and precipitation_intensity > 0
                and metno_icon is not None
            ):
                if icon == Icon.thunderstorm:
                    precipitation_type = PrecipitationType.storm
                elif "rain" in metno_icon or "sleet" in metno_icon:
                    precipitation_type = PrecipitationType.rain
                elif "snow" in metno_icon:
                    precipitation_type = PrecipitationType.snow
                else:
                    precipitation_type = PrecipitationType.rain
            else:
                precipitation_type = None

            hourly_samples.append(
                Sample(
                    sample_type=SampleType.forecast,
                    interval_type=IntervalType.hour,
                    location=Location(
                        authority=None,
                        authority_id=None,
                        geometry={
                            "type": data.geometry.type,
                            "coordinates": data.geometry.coordinates[:2],
                        },
                        name=None,
                    ),
                    valid_from=time_series_element.time,
                    valid_to=time_series_element.time + timedelta(hours=1),
                    computed_at=computed_at,
                    fetched_at=datetime.now(),
                    origin=Origin(
                        name=config.METNO_LOCATION_FORECAST_ORIGIN_NAME,
                        url=config.METNO_LOCATION_FORECAST_ORIGIN_URL,
                        icon=None,
                    ),
                    summary=None,
                    icon=icon,
                    nearest_storm_distance=None,
                    nearest_storm_bearing=None,
                    precipitation_intensity=precipitation_intensity,
                    precipitation_intensity_error=None,
                    precipitation_probability=time_series_element.data.instant.details.probability_of_precipitation,
                    precipitation_type=precipitation_type,
                    temperature=time_series_element.data.instant.details.air_temperature,
                    temperature_min=time_series_element.data.instant.details.air_temperature_min,
                    temperature_max=time_series_element.data.instant.details.air_temperature_max,
                    temperature_min_at=None,
                    temperature_max_at=None,
                    apparent_temperature=None,
                    dew_point=time_series_element.data.instant.details.dew_point_temperature,
                    humidity=time_series_element.data.instant.details.relative_humidity,
                    humidity_min=None,
                    humidity_max=None,
                    humidity_min_at=None,
                    humidity_max_at=None,
                    pressure=time_series_element.data.instant.details.air_pressure_at_sea_level,
                    wind_speed=time_series_element.data.instant.details.wind_speed,
                    wind_speed_max=None,
                    wind_speed_max_at=None,
                    wind_speed_max_bearing=None,
                    wind_gust=time_series_element.data.instant.details.wind_speed_of_gust,
                    wind_bearing=time_series_element.data.instant.details.wind_from_direction,
                    cloud_cover=time_series_element.data.instant.details.cloud_area_fraction,
                    uv_index=time_series_element.data.instant.details.ultraviolet_index_clear_sky,
                    radiation=None,
                    visibility=None,
                    ozone=None,
                    soil_temperatures=None,
                    evapotranspiration=None,
                    source=time_series_element.json(),
                )
            )

        # Time-to-live in the cache
        try:
            response_expires = parsedate_to_datetime(response.headers["Expires"])
            time_to_live = (
                response_expires - datetime.now(tz=timezone.utc)
            ).total_seconds()
        except:
            time_to_live = None

        daily_samples = self.compute_daily_samples(hourly_samples)

        return hourly_samples + daily_samples, time_to_live

    def compute_daily_samples(self, hourly_samples):
        samples_by_date = defaultdict(list)
        for sample in hourly_samples:
            if sample.interval_type != IntervalType.hour:
                continue
            sample_local_time = sample.valid_from.astimezone(
                tz=pytz.timezone(config.TIMEZONE)
            )
            samples_by_date[sample_local_time.date()].append(sample)

        daily_samples = []
        for samples_date, hourly_samples in samples_by_date.items():
            daily_samples.append(
                self.compute_daily_sample(hourly_samples, samples_date)
            )

        return daily_samples

    def compute_daily_sample(self, hourly_samples: list, samples_date: date):
        """Compute daily sample for a given day, from hourly samples

        Args:
            hourly_samples ([Sample]): hourly samples for this day
            date (date): date

        Returns:
            [Sample]: daily samples
        """
        daily_sample = Sample(
            sample_type=hourly_samples[0].sample_type,
            interval_type=IntervalType.day,
            location=hourly_samples[0].location,
        )

        daily_sample.valid_from = datetime(
            samples_date.year,
            samples_date.month,
            samples_date.day,
            0,
            0,
            0,
            tzinfo=pytz.timezone(config.TIMEZONE),
        )

        daily_sample.valid_to = daily_sample.valid_from + timedelta(days=1)

        daily_sample.computed_at = hourly_samples[0].computed_at
        daily_sample.fetched_at = hourly_samples[0].fetched_at

        daily_sample.origin = hourly_samples[0].origin

        daily_sample.icon = Icon.summarize(
            [sample.icon for sample in hourly_samples if sample.icon is not None]
        )

        temperatures = [
            sample.temperature
            for sample in hourly_samples
            if sample.temperature is not None
        ]
        if temperatures:
            daily_sample.temperature_min = round(min(temperatures), 2)
            daily_sample.temperature_min_at = next(
                sample.valid_from
                for sample in hourly_samples
                if sample.temperature == daily_sample.temperature_min
            )
            daily_sample.temperature_max = round(max(temperatures), 2)
            daily_sample.temperature_max_at = next(
                sample.valid_from
                for sample in hourly_samples
                if sample.temperature == daily_sample.temperature_max
            )

        precipitation_intensities = [
            sample.precipitation_intensity
            for sample in hourly_samples
            if sample.precipitation_intensity is not None
        ]
        if precipitation_intensities:
            daily_sample.precipitation_intensity = round(
                sum(precipitation_intensities), 2
            )

        precipitation_probabilities = [
            sample.precipitation_probability
            for sample in hourly_samples
            if sample.precipitation_probability is not None
        ]
        if precipitation_probabilities:
            daily_sample.precipitation_probability = round(
                max(precipitation_probabilities), 2
            )

        precipitation_types = [
            sample.precipitation_type.name
            for sample in hourly_samples
            if sample.precipitation_type is not None
        ]
        if precipitation_types:
            daily_sample.precipitation_type = getattr(
                PrecipitationType,
                max(set(precipitation_types), key=precipitation_types.count),
            )

        humidities = [
            sample.humidity for sample in hourly_samples if sample.humidity is not None
        ]
        if humidities:
            daily_sample.humidity_min = round(min(humidities), 2)
            daily_sample.humidity_min_at = next(
                sample.valid_from
                for sample in hourly_samples
                if sample.humidity == daily_sample.humidity_min
            )
            daily_sample.humidity_max = round(max(humidities), 2)
            daily_sample.humidity_max_at = next(
                sample.valid_from
                for sample in hourly_samples
                if sample.humidity == daily_sample.humidity_max
            )

        wind_speeds = [
            sample.wind_speed
            for sample in hourly_samples
            if sample.wind_speed is not None
        ]
        if wind_speeds:
            daily_sample.wind_speed = round(sum(wind_speeds) / len(wind_speeds), 2)
            daily_sample.wind_speed_max = round(max(temperatures), 2)
            wind_vectors = [
                [
                    sample.wind_speed * cos(radians(90 - sample.wind_bearing)),
                    sample.wind_speed * sin(radians(90 - sample.wind_bearing)),
                ]
                for sample in hourly_samples
                if sample.wind_speed is not None and sample.wind_bearing is not None
            ]
            if wind_vectors:
                daily_sample.wind_bearing = round(
                    90
                    - degrees(
                        atan2(
                            sum([vector[1] for vector in wind_vectors]),
                            sum([vector[0] for vector in wind_vectors]),
                        )
                    ),
                    2,
                )

        return daily_sample


metno_client = MetNoClient()
