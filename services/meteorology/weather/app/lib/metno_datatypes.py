import logging
from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel, confloat


logger = logging.getLogger(__name__)

# LocationForecast API response data model
#
# As per https://api.met.no/doc/locationforecast/datamodel and
# https://api.met.no/doc/ForecastJSON

class MetNoLocationForecastGeometry(BaseModel):
    type: str
    coordinates: List[float]


class MetNoLocationForecastPropertiesMetaUnits(BaseModel):
    air_pressure_at_sea_level: Optional[str] = None
    air_temperature: Optional[str] = None
    air_temperature_max: Optional[str] = None
    air_temperature_min: Optional[str] = None
    cloud_area_fraction: Optional[str] = None
    cloud_area_fraction_high: Optional[str] = None
    cloud_area_fraction_low: Optional[str] = None
    cloud_area_fraction_medium: Optional[str] = None
    dew_point_temperature: Optional[str] = None
    fog_area_fraction: Optional[str] = None
    precipitation_amount: Optional[str] = None
    precipitation_amount_max: Optional[str] = None
    precipitation_amount_min: Optional[str] = None
    probability_of_precipitation: Optional[str] = None
    probability_of_thunder: Optional[str] = None
    relative_humidity: Optional[str] = None
    ultraviolet_index_clear_sky: Optional[str] = None
    wind_from_direction: Optional[str] = None
    wind_speed: Optional[str] = None
    wind_speed_of_gust: Optional[str] = None


class MetNoLocationForecastPropertiesMeta(BaseModel):
    updated_at: datetime
    units: MetNoLocationForecastPropertiesMetaUnits


class MetNoLocationForecastPropertiesTimeseriesElementDataElementSummary(BaseModel):
    symbol_code: Optional[str] = None


class MetNoLocationForecastPropertiesTimeseriesElementDataElementDetails(BaseModel):
    air_pressure_at_sea_level: Optional[confloat(ge=0)] = None
    air_temperature: Optional[float] = None
    air_temperature_max: Optional[float] = None
    air_temperature_min: Optional[float] = None
    air_temperature_percentile_10: Optional[float] = None
    air_temperature_percentile_90: Optional[float] = None
    cloud_area_fraction: Optional[confloat(ge=0, le=100)] = None
    cloud_area_fraction_high: Optional[confloat(ge=0, le=100)] = None
    cloud_area_fraction_low: Optional[confloat(ge=0, le=100)] = None
    cloud_area_fraction_medium: Optional[confloat(ge=0, le=100)] = None
    dew_point_temperature: Optional[float] = None
    fog_area_fraction: Optional[confloat(ge=0, le=100)] = None
    precipitation_amount: Optional[confloat(ge=0)] = None
    precipitation_amount_max: Optional[confloat(ge=0)] = None
    precipitation_amount_min: Optional[confloat(ge=0)] = None
    probability_of_precipitation: Optional[confloat(ge=0, le=100)] = None
    probability_of_thunder: Optional[confloat(ge=0, le=100)] = None
    relative_humidity: Optional[confloat(ge=0, le=100)] = None
    ultraviolet_index_clear_sky: Optional[confloat(ge=0, le=100)] = None
    wind_from_direction: Optional[confloat(ge=0, le=360)] = None
    wind_speed: Optional[confloat(ge=0)] = None
    wind_speed_percentile_10: Optional[confloat(ge=0)] = None
    wind_speed_percentile_90: Optional[confloat(ge=0)] = None
    wind_speed_of_gust: Optional[confloat(ge=0)] = None


class MetNoLocationForecastPropertiesTimeseriesElementDataElement(BaseModel):
    summary: Optional[MetNoLocationForecastPropertiesTimeseriesElementDataElementSummary] = None
    details: Optional[MetNoLocationForecastPropertiesTimeseriesElementDataElementDetails] = None


class MetNoLocationForecastPropertiesTimeseriesElementData(BaseModel):
    instant: MetNoLocationForecastPropertiesTimeseriesElementDataElement
    next_1_hours: Optional[MetNoLocationForecastPropertiesTimeseriesElementDataElement] = None
    next_6_hours: Optional[MetNoLocationForecastPropertiesTimeseriesElementDataElement] = None
    next_12_hours: Optional[MetNoLocationForecastPropertiesTimeseriesElementDataElement] = None


class MetNoLocationForecastPropertiesTimeseriesElement(BaseModel):
    time: datetime
    data: MetNoLocationForecastPropertiesTimeseriesElementData


class MetNoLocationForecastProperties(BaseModel):
    meta: MetNoLocationForecastPropertiesMeta
    timeseries: List[MetNoLocationForecastPropertiesTimeseriesElement]


class MetNoLocationForecastResponse(BaseModel):
    type: str
    geometry: MetNoLocationForecastGeometry
    properties: MetNoLocationForecastProperties
