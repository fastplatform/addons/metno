from pathlib import Path
import glob
import json
from unittest import TestCase

from fastapi.testclient import TestClient

from app.main import app


def test_graphql():

    with TestClient(app) as client:
        filepaths = glob.glob(str(Path(__file__).parent / "graphql/*.graphql"))
        
        for filepath in filepaths:
            filepath = Path(filepath)

            print(">", filepath.name)

            # Read the query and its corresponding expected json
            query = filepath.read_text()
            if filepath.with_suffix(".variables.json").exists():
                variables = json.loads(filepath.with_suffix(".variables.json").read_text())
            else:
                variables = {}

            if filepath.with_suffix(".json").exists():
                expected_json = json.loads(filepath.with_suffix(".json").read_text())
            else:
                expected_json = None

            # Submit and assert results are identical
            response = client.post(
                "/graphql", json={"query": query, "variables": variables}
            )

            if expected_json:
                TestCase().assertDictEqual(response.json(), expected_json)
            else:
                TestCase().assertTrue("errors" not in response.json())
