import os
import sys
from typing import Dict, Union
from pathlib import Path, PurePath

from pydantic import BaseSettings


class Settings(BaseSettings):

    API_DIR: Path = PurePath(__file__).parent / "api"

    EPSG_SRID_ETRS89: str = "4258"
    EPSG_SRID_ETRS89_LAEA: str = "3035"
    EPSG_SRID_WGS84: str = "4326"
    EPSG_SRID_BELGE_72: str = "4313"
    EPSG_SRID_BELGIAN_LAMBERT_72: str = "31370"

    OPENTELEMETRY_EXPORTER_ZIPKIN_ENDPOINT: str = "http://127.0.0.1:9411/api/v2/spans"
    OPENTELEMETRY_SAMPLING_RATIO: float = 0.1
    OPENTELEMETRY_SERVICE_NAME: str = "addon-metno-meteorology-weather"

    TIMEZONE: str = "UTC"

    METNO_USER_AGENT: str = "FaST Platform https://fastplatform.eu tech@fastplatform.eu"
    METNO_DEFAULT_TIMEOUT: int = 10  # seconds
    METNO_LOCATION_FORECAST_URL: str = (
        "https://api.met.no/weatherapi/locationforecast/2.0"
    )
    METNO_LOCATION_FORECAST_CACHE_MAX_SIZE: int = 10000
    METNO_LOCATION_FORECAST_CACHE_TTL: int = 300
    METNO_LOCATION_FORECAST_ORIGIN_NAME: str = "Based on data from MET Norway"
    METNO_LOCATION_FORECAST_ORIGIN_URL: str = "https://met.no"

    NOMINATIM_REVERSE_GEOLOCATION_URL: str = "https://nominatim.openstreetmap.org/reverse"
    NOMINATIM_REVERSE_GEOLOCATION_DEFAULT_TIMEOUT: int = 10  # seconds
    NOMINATIM_REVERSE_GEOLOCATION_USER_AGENT: str = "FaST Platform https://fastplatform.eu tech@fastplatform.eu"
    NOMINATIM_REVERSE_GEOLOCATION_CACHE_MAX_SIZE: int = 10000
    NOMINATIM_REVERSE_GEOLOCATION_CACHE_TTL: int = 300

    LOG_COLORS = bool(os.getenv("LOG_COLORS", sys.stdout.isatty()))
    LOG_FORMAT = str(os.getenv("LOG_FORMAT", "uvicorn"))
    LOG_LEVEL = str(os.getenv("LOG_LEVEL", "info")).upper()
    LOGGING_CONFIG: Dict[str, Union[Dict, bool, int, str]] = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "class": "logging.Formatter",
                "format": "%(levelname)-10s %(message)s",
            },
            "uvicorn": {
                "()": "uvicorn.logging.DefaultFormatter",
                "format": "%(levelprefix)s %(message)s",
                "use_colors": LOG_COLORS,
            },
        },
        "handlers": {
            "default": {
                "class": "logging.StreamHandler",
                "formatter": LOG_FORMAT,
                "level": LOG_LEVEL,
                "stream": "ext://sys.stdout",
            }
        },
        "root": {"handlers": ["default"], "level": LOG_LEVEL},
        "loggers": {
            "fastapi": {"propagate": True},
            "uvicorn": {"propagate": True},
            "uvicorn.access": {"propagate": True},
            "uvicorn.asgi": {"propagate": True},
            "uvicorn.error": {"propagate": True},
        },
    }


config = Settings()
