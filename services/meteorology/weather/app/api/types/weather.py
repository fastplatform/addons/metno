import hashlib
import json

import graphene


class Origin(graphene.ObjectType):
    id = graphene.ID()
    name = graphene.String(
        description="Name of the authority that manages or generates this weather data"
    )
    url = graphene.String(
        description="URL of the authority that manages or generates this weather data"
    )
    icon = graphene.String(
        description="URL of the icon of the authority that manages or generates this weather data"
    )

    def resolve_id(self, info):
        return hashlib.md5(
            json.dumps(
                [
                    self.name,
                    self.url,
                    self.icon,
                ]
            ).encode()
        ).hexdigest()

    class Meta:
        name = "origin"


class Location(graphene.ObjectType):
    id = graphene.ID()
    authority = graphene.String(
        description="Authority that manages or generates this location"
    )
    authority_id = graphene.String(
        description="Identifier of the location within the authority's systems"
    )
    geometry = graphene.JSONString(
        description="Geographical extent of the location (point or polygon)"
    )
    name = graphene.String(description="Name of the location")

    def resolve_id(self, info):
        return hashlib.md5(
            json.dumps(
                [
                    self.authority,
                    self.authority_id,
                    self.geometry,
                    self.name,
                ]
            ).encode()
        ).hexdigest()

    class Meta:
        name = "location"


class Icon(graphene.Enum):
    clear_day = "clear_day"
    clear_night = "clear_night"
    cloudy = "cloudy"
    fog = "fog"
    hail = "hail"
    light_rain = "light_rain"
    light_snow = "light_snow"
    partly_cloudy_day = "partly_cloudy_day"
    partly_cloudy_night = "partly_cloudy_night"
    rain = "rain"
    snow = "snow"
    thunderstorm = "thunderstorm"
    wind = "wind"
    ice = "ice"
    other = "other"

    @staticmethod
    def summarize(icons):
        """
        Summarize a list of icons into a single icon.
        """
        icons = [icon for icon in icons if icon != Icon.other]

        if not icons:
            return None

        # Remove the prefixes and suffixes from the icon name
        summaries = {
            "clear_day": "clear",
            "clear_night": "clear",
            "light_rain": "rain",
            "light_snow": "snow",
            "partly_cloudy_day": "cloudy",
            "partly_cloudy_night": "cloudy",
        }

        icons = [summaries[icon] if icon in summaries else icon for icon in icons]

        # Score the icons and find the one with the highest score
        weights = {
            "ice": 10,
            "snow": 9,
            "hail": 8,
            "thunderstorm": 7,
            "rain": 6,
            "fog": 5,
            "cloudy": 4,
            "wind": 3,
            "clear": 2,
            "other": 0,
        }

        icon_scores = {}
        for icon in icons:
            if icon in icon_scores:
                icon_scores[icon] += weights[icon]
            else:
                icon_scores[icon] = weights[icon]

        max_icon = max(icon_scores, key=icon_scores.get)

        # Special treatment for some prefixes and suffixes
        if max_icon == "rain" and "rain" not in icons:
            return "light_rain"

        if max_icon == "snow" and "snow" not in icons:
            return "light_snow"

        if max_icon == "cloudy" and "cloudy" not in icons:
            return "partly_cloudy_day"

        if max_icon == "clear":
            return "clear_day"

        # Done!
        return max_icon

    class Meta:
        name = "icon"


class IntervalType(graphene.Enum):
    half_hour = "half_hour"
    hour = "hour"
    day = "day"
    week = "week"
    month = "month"

    class Meta:
        name = "interval_type"


class SampleType(graphene.Enum):
    forecast = "forecast"
    observation = "observation"

    class Meta:
        name = "sample_type"


class PrecipitationType(graphene.Enum):
    rain = "rain"
    snow = "snow"
    hail = "hail"
    storm = "storm"

    class Meta:
        name = "precipitation_type"


class SoilTemperature(graphene.ObjectType):
    id = graphene.ID()
    depth = graphene.Float()
    temperature = graphene.Float()

    def resolve_id(self, info):
        return hashlib.md5(
            json.dumps([self.depth, self.temperature]).encode()
        ).hexdigest()

    class Meta:
        name = "soil_temperature"


class Sample(graphene.ObjectType):
    id = graphene.ID()
    sample_type = graphene.NonNull(SampleType)
    interval_type = graphene.NonNull(IntervalType)
    location = graphene.Field(Location)
    valid_from = graphene.NonNull(
        graphene.DateTime,
        description="Start of date/time interval this measure or forecast applies to",
    )
    valid_to = graphene.NonNull(
        graphene.DateTime,
        description="End of date/time interval this measure or forecast applies to",
    )
    computed_at = graphene.DateTime(
        description="Date/time at which the data was computed by the origin authority"
    )
    fetched_at = graphene.DateTime(
        description="Date/time at which the data was fetched from the origin authority"
    )
    origin = graphene.Field(
        Origin,
        description="The organization or authority who generated the measure or forecast",
    )
    summary = graphene.String(description="A text description of the sample")
    icon = graphene.Field(
        Icon, description="An icon (from the FaST nomenclature) describing the sample"
    )
    nearest_storm_distance = graphene.Float(
        description="Distance in meters (m) to the nearest storm, if any"
    )
    nearest_storm_bearing = graphene.Float(
        description="Bearing in degrees (°) to the nearest storm, if any"
    )
    precipitation_intensity = graphene.Float(
        description="Intensity of precipitation, in millimeters per hour (mm/h)"
    )
    precipitation_intensity_error = graphene.Float(
        description="Error on the intensity of precipitation, in millimeters per hour (mm/h)"
    )
    precipitation_probability = graphene.Float(
        description="Probability of precipitation, in percent (%)"
    )
    precipitation_type = graphene.Field(
        PrecipitationType,
        description="Type of precipitation, from the FaST nomenclature",
    )
    temperature = graphene.Float(
        description="Average temperature, in degrees Celsius (°C)"
    )
    temperature_min = graphene.Float(
        description="Minimum temperature, in degrees Celsius (°C)"
    )
    temperature_max = graphene.Float(
        description="Maximum temperature, in degrees Celsius (°C)"
    )
    temperature_min_at = graphene.DateTime(
        description="Date/time of minimum temperature"
    )
    temperature_max_at = graphene.DateTime(
        description="Date/time of maximum temperature"
    )
    apparent_temperature = graphene.Float(
        description="Apparent temperature, in degrees Celsius (°C)"
    )
    dew_point = graphene.Float(description="Dew point, in degrees Celsius (°C)")
    humidity = graphene.Float(description="Average humidity, in percent (%)")
    humidity_min = graphene.Float(description="Minimum humidity, in percent (%)")
    humidity_max = graphene.Float(description="Maximum humidity, in percent (%)")
    humidity_min_at = graphene.DateTime(description="Date/time of minimum humidity")
    humidity_max_at = graphene.DateTime(description="Date/time of maximum humidity")
    pressure = graphene.Float(description="Atmospheric pressure, in hPa")
    wind_speed = graphene.Float(description="Average wind speed, in m/s")
    wind_speed_max = graphene.Float(description="Maximum wind speed, in m/s")
    wind_speed_max_at = graphene.DateTime(description="Date/time of maximum wind speed")
    wind_speed_max_bearing = graphene.Float(
        description="Bearing of wind at maximum speed"
    )
    wind_gust = graphene.Float(description="Speed of wind gusts, in m/s")
    wind_bearing = graphene.Float(description="Average bearing of wind, in degrees (°)")
    cloud_cover = graphene.Float(description="Cloud cover, in percent (%)")
    uv_index = graphene.Float(description="Ultraviolet index")
    radiation = graphene.Float(
        description="Sun radiation, in watts per square meter (w/m2)"
    )
    visibility = graphene.Float(description="Line of sight visibility, in meters (m)")
    ozone = graphene.Float(description="Ozone level")
    soil_temperatures = graphene.List(
        SoilTemperature, description="Temperature of the soil, depending on depth"
    )
    evapotranspiration = graphene.Float(description="Evapotranspiration, in mm/hour")
    source = graphene.JSONString()

    def resolve_id(self, info):
        return hashlib.md5(
            json.dumps(
                [
                    str(self.sample_type),
                    str(self.interval_type),
                    (self.location.id if self.location is not None else None),
                    self.valid_from.isoformat(),
                    self.valid_to.isoformat(),
                    (self.origin.id if self.origin is not None else None),
                    self.summary,
                    str(self.icon),
                    self.nearest_storm_distance,
                    self.nearest_storm_bearing,
                    self.precipitation_intensity,
                    self.precipitation_intensity_error,
                    self.precipitation_probability,
                    str(self.precipitation_type),
                    self.temperature,
                    self.temperature_min,
                    self.temperature_max,
                    (
                        self.temperature_min_at.isoformat()
                        if self.temperature_min_at is not None
                        else None
                    ),
                    (
                        self.temperature_max_at.isoformat()
                        if self.temperature_max_at is not None
                        else None
                    ),
                    self.apparent_temperature,
                    self.dew_point,
                    self.humidity,
                    self.humidity_min,
                    self.humidity_max,
                    (
                        self.humidity_min_at.isoformat()
                        if self.humidity_min_at is not None
                        else None
                    ),
                    (
                        self.humidity_max_at.isoformat()
                        if self.humidity_max_at is not None
                        else None
                    ),
                    self.pressure,
                    self.wind_speed,
                    self.wind_speed_max,
                    (
                        self.wind_speed_max_at.isoformat()
                        if self.wind_speed_max_at is not None
                        else None
                    ),
                    self.wind_speed_max_bearing,
                    self.wind_gust,
                    self.wind_bearing,
                    self.cloud_cover,
                    self.uv_index,
                    self.radiation,
                    self.visibility,
                    self.ozone,
                    (
                        [
                            t.id if t is not None else None
                            for t in self.soil_temperatures
                        ]
                        if self.soil_temperatures is not None
                        else None
                    ),
                    self.evapotranspiration,
                ]
            ).encode()
        ).hexdigest()

    class Meta:
        name = "weather_sample"


# Based on OASIS Common Alerting Protocol
# http://docs.oasis-open.org/emergency/cap/v1.2/CAP-v1.2-os.html


class AlertCategory(graphene.Enum):
    geo = "geo"  # Geophysical (inc. landslide)
    met = "met"  # Meteorological (inc. flood)
    safety = "safety"  # General emergency and public safety
    security = (
        "security"  # Law enforcement, military, homeland and local/private security
    )
    rescue = "rescue"  # Rescue and recovery
    fire = "fire"  # Fire suppression and rescue
    health = "health"  # Medical and public health
    env = "env"  # Pollution and other environmental
    transport = "transport"  # Public and private transportation
    infra = "infra"  # Utility, telecommunication, other non-transport infrastructure
    cbrne = "cbrne"  # Chemical, Biological, Radiological, Nuclear or High-Yield Explosive threat or attack
    other = "other"  # Other events


class AlertUrgency(graphene.Enum):
    immediate = "immediate"
    expected = "expected"
    future = "future"
    past = "past"
    unknown = "unknown"


class AlertSeverity(graphene.Enum):
    extreme = "extreme"
    severe = "severe"
    moderate = "moderate"
    minor = "minor"
    unknown = "unknown"


class AlertCertainty(graphene.Enum):
    observed = "observed"
    likely = "likely"
    possible = "possible"
    unlikely = "unlikely"
    unknown = "unknown"


class Alert(graphene.ObjectType):
    id = graphene.ID()
    sender = graphene.String(
        description="The identifier of the sender of the alert message"
    )
    issued_at = graphene.String(
        description="The effective time of the information of the alert message"
    )
    title = graphene.String(description="The text headline of the alert message")
    summary = graphene.String(description="The text summary of the alert message")
    description = graphene.String(
        description="The text describing the subject event of the alert message"
    )
    url = graphene.String(
        description="The identifier of the hyperlink associating additional information with the alert message"
    )
    category = graphene.NonNull(
        AlertCategory,
        description="The code denoting the category of the subject event of the alert message",
    )
    urgency = graphene.Field(
        AlertUrgency,
        description="The code denoting the urgency of the subject event of the alert message",
    )
    severity = graphene.Field(
        AlertSeverity,
        description="The code denoting the severity of the subject event of the alert message",
    )
    certainty = graphene.Field(
        AlertCertainty,
        description="The code denoting the certainty of the subject event of the alert message",
    )
    location = graphene.NonNull(
        Location, description="The location of the subject event of the alert message"
    )
    valid_from = graphene.NonNull(
        graphene.DateTime,
        description="The effective time of the information of the alert message",
    )
    valid_to = graphene.DateTime(
        description="The expiry time of the information of the alert message"
    )

    def resolve_id(self, info):
        return hashlib.md5(
            json.dumps(
                [
                    self.sender,
                    self.issued_at.isoformat() if self.issued_at is not None else None,
                    self.title,
                    self.summary,
                    self.description,
                    self.url,
                    str(self.category),
                    str(self.urgency),
                    str(self.severity),
                    str(self.certainty),
                    self.location.id if self.location is not None else None,
                    self.valid_from.isoformat()
                    if self.valid_from is not None
                    else None,
                    self.valid_to.isoformat() if self.valid_to is not None else None,
                ]
            ).encode()
        ).hexdigest()

    class Meta:
        name = "alert"
