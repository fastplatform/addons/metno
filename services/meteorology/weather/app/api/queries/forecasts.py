import json
import asyncio

import graphene
from shapely.geometry import shape

from app.api.types.weather import Sample, IntervalType
from app.lib.gis import Projection
from app.lib.metno import metno_client
from app.lib.nominatim import nominatim_client


class Forecasts:

    # Describe capabilities
    forecasts_implemented = graphene.Boolean(default_value=True)
    forecasts_interval_types = graphene.List(IntervalType)

    def resolve_forecasts_interval_types(self, info):
        return [IntervalType.day, IntervalType.hour]

    # Node and resolver for forecasts
    forecasts = graphene.List(
        Sample,
        geometry=graphene.Argument(graphene.String, required=True),
        interval_type=graphene.Argument(IntervalType, required=True),
    )

    async def resolve_forecasts(self, info, geometry, interval_type):
        """Resolver for `forecasts` node

        Arguments:
            info {object} -- GraphQL context
            geometry {str} -- Geometry as a GeoJSON string on ETRS89 datum

        Returns:
            list -- List of weather Samples
        """
        if interval_type not in self.resolve_forecasts_interval_types(info):
            return None

        geometry = shape(json.loads(geometry))
        geometry = Projection.etrs89_to_wgs84(geometry)
        centroid = geometry.centroid

        samples, location_name = await asyncio.gather(
            metno_client.get_location_forecast(
                longitude=centroid.x,
                latitude=centroid.y,
            ),
            nominatim_client.get_location_name(
                longitude=centroid.x, latitude=centroid.y
            ),
        )

        location_name = await nominatim_client.get_location_name(
            longitude=centroid.x, latitude=centroid.y
        )

        for sample in samples:
            sample.location.name = location_name

        return [sample for sample in samples if sample.interval_type == interval_type]
