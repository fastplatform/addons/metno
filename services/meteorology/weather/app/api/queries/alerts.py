import graphene

from app.api.types.weather import Alert


class Alerts:

    # Describe capabilities
    alerts_implemented = graphene.Boolean(default_value=False)

    # Node and resolver for alerts
    alerts = graphene.List(
        Alert, geometry=graphene.Argument(graphene.String, required=True)
    )

    def resolve_alerts(self, info, geometry):
        """Resolver for `alerts` node

        Arguments:
            info {object} -- GraphQL context
            geometry {str} -- Geometry as a GeoJSON string on ETRS89 datum

        Returns:
            list -- List of Alerts
        """
        return []
