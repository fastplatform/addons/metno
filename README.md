# `metno`: Met.no weather service add-on based on the https://met.no public APIs

This addon exposes weather observations and forecasts to users of FaST.

As an additional module of the FaST Platform, it integrates the main service(s) necessary for its operation as well as any other ancillary systems such as databases or cache systems. This repo includes both the [source code](services) and [a descriptive orchestration configuration](manifests) of the entire package.

Services:
* [meteorology/weather](services/meteorology/weather)
